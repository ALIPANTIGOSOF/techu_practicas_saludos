package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1/saludos")
public class ControladorSaludos {

     //GET /api/v1/saludos --> Lista de Saludos
    //POST /api/v1/saludos --> Crea un nuevo Saludo

    //DELETE /api/v1/saludos --> Borra un saludo
    //PUT /api/v1/saludos   --> Reemplazar saludo completo
    //PATH /api/v1/saludos  --> Operaciones para modificar partes de un saludo

    static class Saludo{
        public long id;

        //atributos publicos serializados/desarializados por Spring
        public  String saludoCorto;
        public  String saludoLargo;

    }

    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final List<Saludo> saludos = new LinkedList<Saludo>();

    @GetMapping("/")
    public List<Saludo> devolverSaludos() {
        return this.saludos;
    }
    //GET /api/v1/saludo/{id}
    @GetMapping("/{id}")
    public  Saludo devolverUnSaludo(@PathVariable long id){
        for(Saludo saludo: this.saludos){
            if (saludo.id == id)
                return saludo;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    @PostMapping("/")
    public Saludo agregarSaludo(@RequestBody Saludo saludo){
        saludo.id = this.secuenciaIds.incrementAndGet();
        this.saludos.add(saludo);
        return saludo;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borraSaludo(@PathVariable long id){
        for(int i=0; i<this.saludos.size(); ++i){
            Saludo saludo = this.saludos.get(i);
            if (id == saludo.id){
                this.saludos.remove(i);
                return;
            }
        }
    }
    @PutMapping("/{id}")
    public void reemplazarSaludo(@PathVariable long id, @RequestBody Saludo saludoNuevo){
        for (int i = 0; i< this.saludos.size(); i++){
            Saludo saludo = this.saludos.get(i);
            if(saludo.id == id){
                saludo.saludoCorto = saludoNuevo.saludoCorto;
                saludo.saludoLargo = saludoNuevo.saludoLargo;
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    static  class ParcheSaludo {
       public String operacion; // {"borrar-atributo, "modificar-atributo"}
       public String atributo; // cual atributo borrar
       public Saludo saludo;   // los atributos modificaods para "modificar.atributos".
    }
    @PatchMapping("/{id}")
    public void modificarSaludo(@PathVariable long id, @RequestBody ParcheSaludo parche){
        for (int i = 0; i < this.saludos.size(); i++) {
            Saludo saludo = this.saludos.get(i);
            if (saludo.id == id) {
                emparcharSaludo(saludo, parche);
                return;
            }
        } throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        private Saludo emparcharSaludo(Saludo saludo, ParcheSaludo parche){
        if (parche.operacion.trim().equalsIgnoreCase("borra-atributo")) {
            if ("saludoCorto".equalsIgnoreCase(parche.atributo)){
                saludo.saludoCorto = null;
            }
            if ("saludolargo".equalsIgnoreCase(parche.atributo)){
                saludo.saludoLargo = null;
            }

        } else if (parche.operacion.trim().equalsIgnoreCase("modificar-atributo")){
            if(parche.saludo.saludoCorto != null)
                saludo.saludoCorto = parche.saludo.saludoCorto;
            if(parche.saludo.saludoLargo != null)
                saludo.saludoLargo = parche.saludo.saludoLargo;

        }

          return saludo;
    }
}